# Test Secret Detection CI Component

Serves as a demo project to verify Secret Detection [CI Component](https://gitlab.com/gitlab-components/secret-detection) is working in line with the equivelant [CI template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Secret-Detection.gitlab-ci.yml).
